# **PRÁCTICA N° 8** 
**Univ.:** Melvin Edwin Canaza Castillo

**Docente:** Ing. Jose David Mamani Figueroa 

**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

<hr>

**1.Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos por la terminal.** 

![comandos](IMG/version.png)

<hr>

**2. Realice un video explicando lo siguiente:**
- La creación de una carpeta en el disco local C:, mediante comandos desde la 
terminal
- La creación de un proyecto utilizando React con Next.js


https://drive.google.com/file/d/1FfQad_N0CHKjkY2erEu6kM_tADyEZG-I/view?usp=sharing

<hr>
**3. ¿Qué comandos se utilizó en las anteriores preguntas?**

- Para ver la version de nuestro node y del npm utlizamos el  **node  --version** y **npm --version**
- **npx create-next-app@latest** se utiliza para la instalacion del react.
 
 - Utilizamos los comandos del **cd** para entrar a la carpeta que creamos
 - **npm run dev** se utiliza para ejecutar el react.

<hr>
**4. ¿Qué errores tuvo al crear el proyecto?**

- El error mas comun que pudimos encontrar fue que al momento de instalar react. no nos permite la creacion del archivo, por que se ubica en una carpeta que no existia actualmente.para lo cual tuvimos que crear la carpeta manualmente para que nos deje crear el proyecto.
![comandos2](IMG/1.png)


