# **PRÁCTICA N° 10** 
**Univ.:** Melvin Edwin Canaza Castillo

**Docente:** Ing. Jose David Mamani Figueroa 

**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

<hr>

**LINKS DE LOS FIGMA** 

**https://www.figma.com/design/p72miSDUOj4C6DthrraQVG/Agency---Email-Design-System-For-Agency-(Community)?node-id=187-537&t=F5zZUltIEafHGEat-0**

**https://www.figma.com/design/Y5fgTexPBuOmAIpLjpo8al/Responsive-Landing-Page-Design-%7C-Website-Home-Page-Design-%7C-Agency-Website-UI-Design-(Community)?node-id=204-686&t=H1jGbU5STa9Aalhb-0**

<hr>

**LINKS DE LOS TEMPLATES** 
**https://freebiesbug.com/figma-freebies/tattoo-salon-template/**
**https://freebiesbug.com/figma-freebies/plant-shop-template/**

<hr>

**LINK DEL REPOSITORIO** 
**https://gitlab.com/sistemas3242537/react-with-melvinedwincanazacastillo**

