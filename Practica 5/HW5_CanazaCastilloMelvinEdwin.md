
# Practica Nª5
<hr>
*Diseño y Programación Gráfica*

*Estudiante : Melvin Edwin Canaza Castillo*

<hr>

**nivel 1**
![level 1](img/1.png)
<hr>

**nivel 2**
![level 2](img/2.png)
<hr>

**nivel 3**
![level 3](img/3.png)
<hr>

**nivel 4**
![level 4](img/4.png)
<hr>

**nivel 5**
![level 5](img/5.png)
<hr>

**nivel 6**
![level 6](img/6.png)
<hr>

**nivel 7**
![level 7](img/7.png)
<hr>

**nivel 8**
![level 8](img/8.png)
<hr>

**nivel 9**
![level 9](img/9.png)
<hr>

**nivel 10**
![level 10](img/10.png)
<hr>

**nivel 22**
![level 22](img/22.png)
<hr>

**nivel 23**
![level 23](img/23.png)
<hr>

**nivel 24**
![level 24](img/24.png)
<hr>

**nivel 25**
![level 25](img/25.png)
<hr>

**nivel 26**
![level 26](img/26.png)
<hr>

**nivel 27**
![level 27](img/27.png)
<hr>

**nivel 28**
![level 28](img/28.png)
<hr>

**nivel 29**
![level 29](img/29.png)
<hr>

**nivel 30**
![level 30](img/30.png)
<hr>

**nivel 31**
![level 31](img/31.png)
<hr>

**nivel 32**
![level 32](img/32.png)
<hr>

**niveles**
![niveles](img/niveles.png)